## Technical Assessment

### The application
A Spring boot web application use to edit PJL header files.

### Running the application
Once repository is cloned, import as a maven project into your IDE (with Spring Tool plugin and support) of choice. Spring Tool Suite recommended.

Run the test suite class (TestSuiteClass.java)

Run the project as a Spring Boot App or run PrintstreamApplication.java as a Java application. Alternatively, please build the project using mvn install or mvn package and deploy the war file (rename to printstream.war) in the webapps folder of your local Tomcat repository.

### Navigating the application

1.) If project is run in IDE open http://localhost:8080/home to a browser. Otherwise, if project is deployed in a standalone Tomcat server access it at http://localhost:8080/printstream/home. This should take you to the home page

![image](https://bitbucket.org/kenlegaspi/ricoh-assessment/raw/master/src/test/resources/screenshots/home.png)

2.) Click Check Directory to display the files used. 

![screenshot](https://bitbucket.org/kenlegaspi/ricoh-assessment/raw/master/src/test/resources/screenshots/checkDirectory.png)

3.) You may select 1 or more .pjl files to edit. When selecting a single file, click on the label (e.g. sample1.pjl). Otherwise, tick the checkboxes and click "Display Headers"

![image](https://bitbucket.org/kenlegaspi/ricoh-assessment/raw/master/src/test/resources/screenshots/individualFile.png)

4.) Please select USERID from the dropdown list. It should display the current value from the file. Update the value as needed.

![image](https://bitbucket.org/kenlegaspi/ricoh-assessment/raw/master/src/test/resources/screenshots/updateHeader.png)

5.) Click "Update Headers" to update the specific header. Output file (e.g new_sample1.pjl) should be created inside the directory where you have imported the project as shown below.

   E.g: /Users/kenlegaspi/RICOH/ricoh-assessment/**src/test/outputs**

![image](https://bitbucket.org/kenlegaspi/ricoh-assessment/raw/master/src/test/resources/screenshots/updateSuccessful.png)

6.) Note that this would automatically be saved as a new file with the filename new_[original filename].pjl

7.) Also, the original file (e.g sample1.pjl) would then be removed from the samples folder and moved to the process folder. 

   E.g: /Users/kenlegaspi/RICOH/ricoh-assessment/**src/test/processed**